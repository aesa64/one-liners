document.body.innerHTML = '<div id="jokes"></div>';

const { jokes } = require('../main');

test('The question should be a <h2> element.', function() {
    for (var j = 0; j < jokes.length; j++) {
        expect( jokes[j].question).toMatch(/<h2>/);
    } 
});

test('The answer should be a <h3> element.', function() {
    for (var a = 0; a < jokes.length; a++) {
        expect( jokes[a].answer).toMatch(/<h3>/);
    } 
});

test('The answer should start with a - followed by a space.', function() {
    for (var i = 0; i < jokes.length; i++) {
        expect( jokes[i].answer).toMatch(/- /);
    } 
});
